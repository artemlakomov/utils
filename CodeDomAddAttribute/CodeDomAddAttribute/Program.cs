﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.NRefactory.CSharp;
using ICSharpCode.NRefactory.CSharp.Refactoring;
using ICSharpCode.NRefactory.CSharp.Resolver;

namespace CodeDomAddAttribute
{
    class Program
    {
        static string sourcePath = @"C:\Projects\branches\NineToFive.Data";
        static string destinationPath = "";
        static int fileCount = 0;
        static CodeDomProvider provider = new CSharpCodeProvider();
        static AttributeSection attribute;
        static CSharpFormatter formatter = new CSharpFormatter( FormattingOptionsFactory.CreateAllman() );

        static void Main( string[] args )
        {
            destinationPath = Path.Combine( Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ), "output" );
            if ( Directory.Exists( destinationPath ) )
                Directory.Delete( destinationPath, true );
            Directory.CreateDirectory( destinationPath );

            DirectoryInfo di = new DirectoryInfo( sourcePath );
            foreach ( var fi in di.EnumerateFileSystemInfos() )
            {
                ProcessEntry( fi );
            }
            Console.WriteLine( "" );
            Console.WriteLine( "Done {0} files. Press ENTER to exit...", fileCount );
            Console.ReadLine();
        }

        static void ProcessEntry( FileSystemInfo fi )
        {
            if ( fi.Attributes == FileAttributes.Directory )
            {
                foreach ( var ci in ( new DirectoryInfo( fi.FullName ) ).EnumerateFileSystemInfos() )
                {
                    ProcessEntry( ci );
                }
            }
            else
            {
                if ( fi.Extension == ".cs" )
                {

                    using ( StreamReader reader = File.OpenText( fi.FullName ) )
                    {
                        ICSharpCode.NRefactory.Editor.StringBuilderDocument doc = new ICSharpCode.NRefactory.Editor.StringBuilderDocument( reader.ReadToEnd() );

                        using ( var docreader = doc.CreateReader() )
                        {

                            CSharpParser parser = new CSharpParser();
                            SyntaxTree tree = parser.Parse( docreader );
                            bool needsAttribute = false;
                            foreach ( var classDeclaration in tree.Descendants.OfType<ICSharpCode.NRefactory.CSharp.TypeDeclaration>() )
                            {
                                if ( classDeclaration.ClassType == ClassType.Class && !classDeclaration.Attributes.Any( x => x.ToString().Contains( "Serializable" ) ) )
                                {
                                    Console.Write( "{0}. {1}. Processing...", fi.Name, classDeclaration.Name.ToString() );

                                    foreach ( var baseType in classDeclaration.BaseTypes )
                                    {
                                        string typeName = baseType.ToString();
                                        if ( typeName.StartsWith( "_" ) || typeName == "SqlClientEntity" )
                                        {
                                            needsAttribute = true;
                                            break;
                                        }
                                    }

                                    if ( needsAttribute )
                                    {
                                        fileCount++;
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.Write( " Needs attribute " );
                                        Console.ForegroundColor = ConsoleColor.White;

                                        //classDeclaration.Attributes.Add( GetSerializableAttribute() ); doc.                                       
                                        doc.Insert( doc.GetOffset( classDeclaration.StartLocation ), GetSerializableAttribute().ToString() + "\t", ICSharpCode.NRefactory.Editor.AnchorMovementType.BeforeInsertion );
                                        
                                        if ( doc.IndexOf( "System;", 0, doc.GetOffset( classDeclaration.StartLocation ), StringComparison.OrdinalIgnoreCase ) < 0 )
                                            doc.Insert( 0, "using System;\r\n", ICSharpCode.NRefactory.Editor.AnchorMovementType.BeforeInsertion);

                                        Console.ForegroundColor = ConsoleColor.Green;
                                        Console.WriteLine( "Done." );
                                        Console.ForegroundColor = ConsoleColor.White;

                                        break;
                                    }
                                                                        
                                    Console.WriteLine( "Done." );                                   
                                }
                            }


                            if ( needsAttribute )
                            {
                                //var csharp = tree.ToString();
                                var newPath = fi.FullName.Replace( sourcePath, destinationPath );
                                var dir = Path.GetDirectoryName( newPath );
                                if ( !Directory.Exists( dir ) )
                                    Directory.CreateDirectory( dir );
                                using ( StreamWriter sw = File.CreateText( newPath ) )
                                {
                                    doc.WriteTextTo( sw );
                                }
                                //File.WriteAllText( newPath, formatter.Format( csharp ), Encoding.UTF8 );
                            }
                        }
                    }


                }
            }
        }

        static AttributeSection GetSerializableAttribute()
        {
            if ( null == attribute )
                using ( StreamReader reader = File.OpenText( Path.Combine( sourcePath, "BL\\Attachment.cs" ) ) )
                {
                    CSharpParser parser = new CSharpParser();
                    SyntaxTree tree = parser.Parse( reader.ReadToEnd() );
                    foreach ( var classDeclaration in tree.Descendants.OfType<ICSharpCode.NRefactory.CSharp.TypeDeclaration>() )
                    {
                        foreach ( var a in classDeclaration.Attributes )
                        {
                            if ( a.ToString().Contains( "Serializable" ) )
                            {
                                attribute = a;
                            }
                        }
                    }
                }
            return attribute.Clone() as AttributeSection;
        }

    }
}
